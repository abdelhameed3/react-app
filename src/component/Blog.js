import React, { Component } from 'react';
import axios from 'axios';

class Blog extends Component {
    state = {
        users: []
    }
    componentDidMount() {
        axios.get("https://jsonplaceholder.typicode.com/users")
            .then(res => {
                this.setState({
                    users: res.data
                })
            })
    }
    render() {
        const { users } = this.state; //  == const users = this.state.users
        const userList = users.map(user => {
            return (
                <div key={user.id}>
                    <div className="users">
                        <p>  Names: {user.name}</p>
                        <p>User name: {user.username}</p>
                    </div>
                </div>
            )
        })
        return (
            <div>
                {userList}
            </div>
        )
    }
}

export default Blog;
