import  React from 'react';
import {Link, NavLink} from 'react-router-dom'

const Nav = () => {
        return(
            <nav>
                <NavLink activeClassName="test" exact to="/">Home</NavLink> <br/>
                <NavLink activeClassName="test" to="/about">About</NavLink> <br/>
                <NavLink activeClassName="test" to="/blog">Blog</NavLink>
            </nav>
        )
}
export default Nav;
