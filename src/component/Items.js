import React, { Component } from 'react';
import './items.css';
class Childitem extends Component {

    render(){
        const {SendItems} = this.props;
        const getItems = SendItems.map( (item) => {
            // if(item.id > 1){
            //     return(
            //         <div key={item.id}>
            //             <p>{item.id} ====> {item.name} ====> {item.age}</p>
            //         </div>
            //     )
            // }

            return(item.age>20 ? 
                <div key={item.id}>
                       <p>{item.id} ====> {item.name} ====> {item.age}</p>
                   </div>
                   : null
                )
            
        } )
        return(
            <div>{getItems}</div>
        )
    }
}


export default Childitem