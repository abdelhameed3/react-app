import React , { Component }from 'react';
import './App.css';
import test from './mod';
import Child from './child';
import Childitem from './component/Items'
class  App extends Component {
  constructor(){
    super();
    console.log("constructor");
  }
  componentDidMount(){
    console.log("componentDidMount");
  }
  componentDidUpdate(prevProps,prevState){
    console.log("componentDidUpdate",prevState);
  }
  // state = {
  //   name: "abdelhameed",
  //   age: 23
  // }
  /* Arrow function */
  Test2 = () => {
    console.log("Ddfd");
  }
  handelClick(){
    console.log('Click');
  }

  /* changeState */
  // changeState = () =>{   // Using arrow function to  make *this* Keyword refrence to classparent (app)
  //   this.setState({
  //     name: " Hassna",
  //     age: 20
  //   })
  // }

  state = {
    Items: [
      {id:0, name:"Abdo",age:35},
      {id:1, name:"Hassna",age:35},
      {id:2, name:"Adam",age:10},
      {id:3, name:"Gana",age:5}
    ],
    test: "sss"
  }

  handelChange = (e) => {
    
    this.setState({
      test: e.target.value
    })
  }
  handelSubmit = (e) => {
    e.preventDefault();
    console.log(this.state.test );
  }
  handelAdd = (e) =>{
    let rend = Math.random()*10;

    let items = this.state.Items;
    items.push({id:rend,name:"hisham",age:100})
    this.setState({
      Items: items
    })
  }
  handleDelete = (e) =>{

    this.setState({
      Items: [
        {id:2,name:"hisham",age:100}
      ]
    })
  }
 render() {
   console.log('render');
  return (
    <div className="App">
     {/* <Child />
     <button onClick={this.handelClick} onMouseMove={this.handelClick}>Event</button>
     {this.Test2()}
      <button onDoubleClick={this.changeState}>Change state</button>
     <p>{this.state.name} ===> {this.state.age}</p> */}

     <Childitem SendItems={this.state.Items}/>

     <form onSubmit= {this.handelSubmit}>
       <input type="text" onChange ={this.handelChange} />
     </form>
     <button onClick={this.handelAdd}>Add</button>
     <button onClick={this.handleDelete}>Delete</button>
     {this.state.test}
    </div>
  );
 }
}

export default App;
