import React , { Component }from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Nav from "./component/Nav";
import Home from "./component/Home";
import About from "./component/About";
import Blog from "./component/Blog";
import Rout from "./component/Rout";

class  Main extends Component {
    render() {
        return(
            <BrowserRouter>
                <section>
                    <Nav/>
                    <Switch>
                        <Route exact path="/" component={Home}/> {/*exact For / */}
                        <Route path="/about" component={About}/>
                        <Route path="/blog" component={Blog}/>
                        <Route path="/:paramsTest" component={Rout}/>
                    </Switch>
                </section>
            </BrowserRouter>
        )
    }
}


export default Main;
